## **cotw-exercises**
A simple container project for various Coders of the West exercises

## Prerequisites

* Atlassian Sourcetree
* A code editor: Atom, Komodo, or Notepad++